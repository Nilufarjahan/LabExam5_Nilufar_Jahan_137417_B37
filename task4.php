<?php

 class factorial_of_a_number
 {
     protected $_number;

     public function __construct($number)
     {
         if (!is_int($number)) {
             throw new InvalidArgumentException('Not a number or missing argument');
         }

         $this->_number = $number;

     }

     public function result()
     {
         $value = 1;
         $i = $this->_number;
         for ($i; $i >= 1; $i--)
             $value = $value * $i;
         return $value;
     }

 }


$newfactorial = New factorial_of_a_number(5);

echo $newfactorial->result();
?>